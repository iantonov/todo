define(function(require){

    var angular = require('angular'),
        Controllers = angular.module('controllers', []);

    Controllers.controller('TodoCtrl', require('controllers/TodoCtrl'));

    return Controllers;
})
